import {Router} from 'express';
import supplieServiceController from '../controllers/supplieServiceController';
class SupplieServiceRouters{
    router: Router = Router();
    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/supplie/service',supplieServiceController.allSupplieService);
        this.router.post('/supplie/service',supplieServiceController.createSupplieService);
        this.router.get('/supplie/service/:id',supplieServiceController.getSupplieService);
        this.router.put('/supplie/sercice/:id',supplieServiceController.updateSupplieService);
        this.router.delete('/supplie/service/:id',supplieServiceController.deleteSupplieService);
    }
}
const supplieServiceRouters = new SupplieServiceRouters();
export default supplieServiceRouters.router;