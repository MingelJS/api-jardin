import {Router} from 'express';
import groupController from '../controllers/groupController';
class GroupRouters{
    router: Router = Router();
    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/group',groupController.allGroups);
        this.router.post('/group',groupController.createGroup);
        this.router.get('/group/:id',groupController.getGroup);
        this.router.put('/group/:id',groupController.updateGroup);
        this.router.delete('/group/:id',groupController.deleteGroup);
        this.router.get('/group/user/:id',groupController.UsersGroups);
    }
}
const groupRouters = new GroupRouters();
export default groupRouters.router;