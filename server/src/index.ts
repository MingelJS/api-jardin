import express,{Application} from 'express';
import userRoutes from './routes/userRoutes';
import suppliesRoutes from './routes/suppliesRoutes';
import solicitudeRoutes from './routes/solicitudeRoutes';
import groupRoutes from './routes/groupRoutes';
import serviceRoute from './routes/serviceRoute';
import solicitudeServiceRoute from './routes/solicitudeServiceRoute';
import suppliesServiceRoutes from './routes/suppliesServiceRoutes'
import morgan from 'morgan';
import cors  from 'cors';

class Server {
    public app: Application;
    constructor(){
        this.app = express();
        this.config();
        this.routes();
    }
    config(): void{
        this.app.set('port',process.env.port || 3000);
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
    }
    routes(): void{
        this.app.use('/api',userRoutes);
        this.app.use('/api',suppliesRoutes);
        this.app.use('/api/',solicitudeRoutes);
        this.app.use('/api/',groupRoutes);
        this.app.use('/api/',serviceRoute);
        this.app.use('/api/',solicitudeServiceRoute);
        this.app.use('/api/',suppliesServiceRoutes);
    }
    start(){
        this.app.listen(this.app.get('port'),()=>{
            console.log('server on port', this.app.get('port'));
        });
    }
}

const server = new Server();
server.start();