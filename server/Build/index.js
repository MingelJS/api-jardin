"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userRoutes_1 = __importDefault(require("./routes/userRoutes"));
const suppliesRoutes_1 = __importDefault(require("./routes/suppliesRoutes"));
const solicitudeRoutes_1 = __importDefault(require("./routes/solicitudeRoutes"));
const groupRoutes_1 = __importDefault(require("./routes/groupRoutes"));
const serviceRoute_1 = __importDefault(require("./routes/serviceRoute"));
const solicitudeServiceRoute_1 = __importDefault(require("./routes/solicitudeServiceRoute"));
const suppliesServiceRoutes_1 = __importDefault(require("./routes/suppliesServiceRoutes"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        this.app.set('port', process.env.port || 3000);
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
    routes() {
        this.app.use('/api', userRoutes_1.default);
        this.app.use('/api', suppliesRoutes_1.default);
        this.app.use('/api/', solicitudeRoutes_1.default);
        this.app.use('/api/', groupRoutes_1.default);
        this.app.use('/api/', serviceRoute_1.default);
        this.app.use('/api/', solicitudeServiceRoute_1.default);
        this.app.use('/api/', suppliesServiceRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('server on port', this.app.get('port'));
        });
    }
}
const server = new Server();
server.start();
