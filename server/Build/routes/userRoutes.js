"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const userController_1 = __importDefault(require("../controllers/userController"));
class UserRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/user', userController_1.default.allUser);
        this.router.post('/user', userController_1.default.createUser);
        this.router.get('/user/:id', userController_1.default.getUser);
        this.router.put('/user/:id', userController_1.default.updateUser);
        this.router.delete('/user/:id', userController_1.default.deleteUser);
        this.router.post('/user/login/', userController_1.default.login);
    }
}
const userRouters = new UserRouters();
exports.default = userRouters.router;
