"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const supplieServiceController_1 = __importDefault(require("../controllers/supplieServiceController"));
class SupplieServiceRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/supplie/service', supplieServiceController_1.default.allSupplieService);
        this.router.post('/supplie/service', supplieServiceController_1.default.createSupplieService);
        this.router.get('/supplie/service/:id', supplieServiceController_1.default.getSupplieService);
        this.router.put('/supplie/sercice/:id', supplieServiceController_1.default.updateSupplieService);
        this.router.delete('/supplie/service/:id', supplieServiceController_1.default.deleteSupplieService);
    }
}
const supplieServiceRouters = new SupplieServiceRouters();
exports.default = supplieServiceRouters.router;
