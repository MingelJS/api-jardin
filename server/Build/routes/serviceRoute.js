"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const serviceController_1 = __importDefault(require("../controllers/serviceController"));
class ServiceRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/service', serviceController_1.default.allServices);
        this.router.post('/service', serviceController_1.default.createService);
        this.router.get('/service/:id', serviceController_1.default.getService);
        this.router.put('/service/:id', serviceController_1.default.updateService);
        this.router.delete('/service/:id', serviceController_1.default.deleteService);
    }
}
const serviceRouters = new ServiceRouters();
exports.default = serviceRouters.router;
