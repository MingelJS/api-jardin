"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class SuppliesController {
    allSupplies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from supplies');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    getSupplie(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from supplies where id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    createSupplie(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const supplies_description = req.param('supplies_description');
            const supplies_name = req.param('supplies_name');
            try {
                yield database_1.dataBase.simpleExecute('insert into supplies values (:supplies_description,:supplies_name,supplies_seq.NEXTVAL)', [supplies_description, supplies_name], { autoCommit: true });
                res.json('supplie creado');
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    deleteSupplie(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from supplies where id = :id', [id], { autoCommit: true });
                res.json({ text: 'sup eliminado' });
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    updateSupplie(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            const description = req.param('supplies_description');
            const name = req.param('supplies_name');
            try {
                yield database_1.dataBase.simpleExecute('update supplies set supplies_description = :1, supplies_name = :2 where id = :id', [description, name, id], { autoCommit: true });
                res.json('Supplie modificado con exito');
            }
            catch (error) {
                console.log('Error: ', error);
            }
        });
    }
}
const suppliesController = new SuppliesController();
exports.default = suppliesController;
