"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class SolicitudeServiceController {
    allSolicitudeService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            try {
                console.log("successfully connected to Oracle!");
                let result = yield database_1.dataBase.simpleExecute('select * from solicitude_service');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    getSolicitudeService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                console.log("successfully connected to Oracle!");
                let result = yield database_1.dataBase.simpleExecute('select * from solicitude_service where solicitude_solicitude_id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    createSolicitudeService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const address = req.param('address');
            const status = req.param('status');
            const date = new Date();
            const solicitude_id = req.param('solicitude_solicitude_id');
            const service_id = req.param('service_service_id');
            try {
                yield database_1.dataBase.simpleExecute('insert into solicitude_service values (:1,:2,:3,:4,:5)', [address, status, date, solicitude_id, service_id], { autoCommit: true });
                res.json('solicitud/servicio creado');
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    deleteSolicitudeService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from solicitude_service where solicitude_solicitude_id = :id', [id], { autoCommit: true });
                res.json({ text: 'solicitud eliminada' });
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    updateSolicitudeService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            const address = req.param('address');
            const status = req.param('status');
            const solicitude_id = req.param('solicitude_solicitude_id');
            const service_id = req.param('service_service_id');
            try {
                yield database_1.dataBase.simpleExecute('update solicitude_service set address = :1, status = :2, solicitude_solicitude_id = :4, service_service_id =:5 where solicitude_solicitude_id = :id', [address, status, solicitude_id, service_id], { autoCommit: true });
                res.json('Solicitud/servicio modificado con exito');
            }
            catch (error) {
                console.log('Error: ', error);
            }
        });
    }
}
const solicitudeServiceController = new SolicitudeServiceController();
exports.default = solicitudeServiceController;
