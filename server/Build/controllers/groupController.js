"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class GroupController {
    allGroups(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from "group"');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    getGroup(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from "group" where id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    createGroup(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const name = req.param('group_name');
            const status = req.param('gruop_status_id');
            const description = req.body.group_description;
            console.log(name);
            try {
                yield database_1.dataBase.simpleExecute('insert into "group" values (group_seq.NEXTVAL,:group_name,:status,:description)', [name, status, description], { autoCommit: true });
                res.json('Grupo creado');
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    deleteGroup(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from "group" where id = :id', [id], { autoCommit: true });
                res.json('grupo eliminado');
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    updateGroup(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const name = req.param('group_name');
            const status = req.param('group_status_id');
            try {
                yield database_1.dataBase.simpleExecute('update "group" set group_name = :1, gruop_status_id = :2 where id = :id', [name, status, id], { autoCommit: true });
                res.json('Grupo modificado con exito');
            }
            catch (error) {
                console.log('Error: ', error);
            }
        });
    }
    UsersGroups(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from "user" where group_id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
}
const groupController = new GroupController();
exports.default = groupController;
