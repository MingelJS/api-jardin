"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class SolicitudeController {
    allSolicitude(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log("successfully connected to Oracle!");
                let result = yield database_1.dataBase.simpleExecute('select * from solicitude');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
            finally {
                if (database_1.dataBase) {
                    try {
                        yield database_1.dataBase.close();
                        console.log('connection closed');
                    }
                    catch (error) {
                        console.log('Error when closing the database connection: ', error);
                    }
                }
            }
        });
    }
    getSolicitude(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                console.log("successfully connected to Oracle!");
                let result = yield database_1.dataBase.simpleExecute('select * from solicitude where solicitude_id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
            finally {
                if (database_1.dataBase) {
                    try {
                        yield database_1.dataBase.close();
                        console.log('connection closed');
                    }
                    catch (error) {
                        console.log('Error when closing the database connection: ', error);
                    }
                }
            }
        });
    }
    createSolicitude(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const status = req.param('solicitude_status');
            const user_id = req.param('user_user_id');
            try {
                console.log('successfully connected to Oracle!');
                yield database_1.dataBase.simpleExecute('insert into solicitude values (solicitude_seq.NEXTVAL,:1,:2,0)', [status, user_id], { autoCommit: true });
                res.json('solicitud creada');
            }
            catch (error) {
                console.log(error);
                res.json(400);
            }
            finally {
                if (database_1.dataBase) {
                    try {
                        yield database_1.dataBase.close();
                        console.log('Connection Closed');
                    }
                    catch (error) {
                        console.log('Error when closing the database connection: ', error);
                    }
                }
            }
        });
    }
    deleteSolicitude(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from solicitude where solicitude_id = :id', [id], { autoCommit: true });
                res.json({ text: 'solicitud eliminada' });
            }
            catch (error) {
                console.log('"Error: ', error);
            }
            finally {
                if (database_1.dataBase) {
                    try {
                        yield database_1.dataBase.close();
                        console.log('connection closed');
                    }
                    catch (error) {
                        console.log('Error when closing the database connection: ', error);
                    }
                }
            }
        });
    }
    updateSolicitude(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            const status = req.param('solicitude_status');
            const user_id = req.param('user_user_id');
            const grupo_id = req.body.SOLICITUDE_GROUP_ID;
            try {
                yield database_1.dataBase.simpleExecute('update solicitude set solicitude_status = :1, user_user_id = :2,solicitude_group_id = :3 where solicitude_id = :id', [status, user_id, grupo_id], { autoCommit: true });
                res.json('Solicitud modificado con exito');
            }
            catch (error) {
                console.log('Error: ', error);
            }
            finally {
                if (conn) {
                    try {
                        yield database_1.dataBase.close();
                        console.log('connection closed');
                    }
                    catch (error) {
                        console.log('Error when closing the database connection: ', error);
                    }
                }
            }
        });
    }
}
const solicitudeController = new SolicitudeController();
exports.default = solicitudeController;
