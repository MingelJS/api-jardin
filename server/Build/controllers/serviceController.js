"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class ServiceController {
    allServices(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from service ');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    getService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from service where service_id = :1', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    createService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const name = req.param('service_description');
            const description = req.param('service_name');
            try {
                yield database_1.dataBase.simpleExecute('insert into service values (service_seq.NEXTVAL,:1,:2)', [description, name], { autoCommit: true });
                res.json('Servicio creado');
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    deleteService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from service where service_id = :id', [id], { autoCommit: true });
                res.json({ text: 'servicio eliminado' });
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    updateService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const name = req.param('service_name');
            const description = req.param('service_description');
            try {
                yield database_1.dataBase.simpleExecute('update service set service_name = :1, service_description = :2 where service_id = :id', [name, description, id], { autoCommit: true });
                res.json('Servicio modificado con exito');
            }
            catch (error) {
                console.log('Error: ', error);
            }
        });
    }
}
const serviceController = new ServiceController();
exports.default = serviceController;
